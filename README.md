# Git Task

1) Git Lab repo was created by the name Git Tasks
2) **Hello World.cpp** c++ code was commited and pushed
3) 3 branches were created by the name
   - `development`
   - `feature/feat_1`
   - `feature/feat_2`
4) Issues were created
5) Issues were resolved using CLI
6) Branches were rebased
7) Branches were merged
8) Readme was edited